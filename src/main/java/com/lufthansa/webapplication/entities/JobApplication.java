package com.lufthansa.webapplication.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "job_application")
public class JobApplication {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long id;

    @ManyToOne
    @JoinColumn(name = "jobApplicationId", referencedColumnName = "applicationId")
    private Application application;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "job_id")
    private Job job;

    private Integer amountOfApplication;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JobApplication)) return false;
        JobApplication that = (JobApplication) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getApplication(), that.getApplication()) &&
                Objects.equals(getJob(), that.getJob()) &&
                Objects.equals(getAmountOfApplication(), that.getAmountOfApplication());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getApplication(), getJob(), getAmountOfApplication());
    }
}

package com.lufthansa.webapplication.services;

import com.lufthansa.webapplication.entities.Role;
import com.lufthansa.webapplication.entities.User;
import com.lufthansa.webapplication.model.constants.ErrorMessages;
import com.lufthansa.webapplication.model.dto.UserDto;
import com.lufthansa.webapplication.repositories.UserRepository;
import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
@Slf4j
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private  UserService userService;


    @Autowired
    BCryptPasswordEncoder passwordEncoder;

    public User saveTheUser(final UserDto userDto) {
        User user = new User();
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setUsername(userDto.getUsername());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        userRepository.save(user);
        UserService.log.info("User saved successfully");
        return user;
    }

    public User updateTheUser(final UserDto userDto)  {
        User user = this.userService.getLoggedInUser();
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        userRepository.save(user);
        UserService.log.info("User updated successfully");
        return user;
    }


    public List<User> readAllTheUsers() {
        return this.userRepository.findAll();
    }

    public User readUserById(final Long userId) throws NotFoundException {
        return this.userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundException(ErrorMessages.ID_NOT_FOUND_EXCEPTION));
    }

    public void deleteAllUsers() {
        this.userRepository.deleteAll();
    }

    public void deleteUserById(final Long id) throws NotFoundException {
        User user = this.userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(ErrorMessages.ID_NOT_FOUND_EXCEPTION));
        this.userRepository.delete(user);
    }


    public User findByUsername(String username) {

        return userRepository.findByUsername(username);
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = findByUsername(username);
        if (user == null){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        for (Role role : user.getUserRole()){
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getRoleName()));
        }

        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), grantedAuthorities);
    }

    public User getLoggedInUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        return this.userRepository.findByUsername(name);
    }
}

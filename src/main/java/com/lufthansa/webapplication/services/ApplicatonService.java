package com.lufthansa.webapplication.services;


import com.lufthansa.webapplication.entities.Application;
import com.lufthansa.webapplication.entities.JobApplication;
import com.lufthansa.webapplication.entities.User;
import com.lufthansa.webapplication.model.constants.ErrorMessages;
import com.lufthansa.webapplication.repositories.ApplicationRepository;
import com.lufthansa.webapplication.repositories.JobApplicationRepository;
import com.lufthansa.webapplication.utils.ApplicationContainer;
import com.lufthansa.webapplication.utils.ApplyContainer;
import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

@Service
@Slf4j
public class ApplicatonService {

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private UserService userService;


    @Autowired
    private JobApplicationRepository jobApplicationRepository;

    @Autowired
    private ApplicationContainer applicationContainer;


    public List<Application> readAllTheApplications() {
        return this.applicationRepository.findAll();
    }

    public Application readApplicationById(final Long applicationId) throws NotFoundException {
        return this.applicationRepository.findById(applicationId)
                .orElseThrow(() -> new NotFoundException(ErrorMessages.ID_NOT_FOUND_EXCEPTION));
    }


        @Transactional
        public Object submitOrderForApplication() {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String name = auth.getName();
            Application application =new Application();
            User user = userService.findByUsername(name);

            applicationContainer.getApplications().setUser(user);
            applicationRepository.save(applicationContainer.getApplications());
            jobApplicationRepository.saveAll(applicationContainer.getJobApplications());

            ApplicatonService.log.info("Applied successfully");
            return application;
        }

        public List<ApplyContainer> getApplicationsUser(){
            User user = userService.getLoggedInUser();
            List<Application> applicationList = applicationRepository.findByUser(user);

            List<ApplyContainer> result = new LinkedList<>();
            for (Application application : applicationList){
                List<JobApplication> jobApplicationList = jobApplicationRepository.findByApplication(application);
                result.add(new ApplyContainer(application, jobApplicationList));
            }
            return result;
        }
}

package com.lufthansa.webapplication.services;


import com.lufthansa.webapplication.entities.Job;
import com.lufthansa.webapplication.entities.User;
import com.lufthansa.webapplication.model.constants.ErrorMessages;
import com.lufthansa.webapplication.model.dto.JobDto;
import com.lufthansa.webapplication.repositories.JobRepository;
import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class JobService {
    @Autowired
    private JobRepository jobRepository;


    @Autowired
    private UserService userService;

    public Job findJobById(final long jobId) throws NotFoundException {
        return this.jobRepository.findById(jobId)
                .orElseThrow(() -> new NotFoundException(ErrorMessages.ID_NOT_FOUND_EXCEPTION));
    }

    public Job saveJob (final JobDto jobDto){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        User user = userService.findByUsername(name);
        Job job= new Job();
        job.setJobName(jobDto.getJobName());
        job.setDescription(jobDto.getDescription());
        jobRepository.save(job);
        JobService.log.info("Job saved successfully");
        return  job;
    }

    public List<Job> readAllJobs(){
        return this.jobRepository.findAll();
    }

    public Job updateJob (final JobDto jobDto, final Long jobId) throws NotFoundException {
        Job job= this.jobRepository.findById(jobId)
                .orElseThrow(() -> new NotFoundException(ErrorMessages.ID_NOT_FOUND_EXCEPTION));
        User user = this.userService.getLoggedInUser();

        job.setJobName(jobDto.getJobName());
        job.setDescription(jobDto.getDescription());
        jobRepository.save(job);
        JobService.log.info("Job updated successfully");
        return  job;
    }
    public void deleteAllJobs() {
        this.jobRepository.deleteAll();
    }

    public void deleteJobById(final Long id) throws NotFoundException {
        Job job = this.jobRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(ErrorMessages.ID_NOT_FOUND_EXCEPTION));
        this.jobRepository.delete(job);
    }


    public List<Job> searchByDescription(final String description) {
       return this.jobRepository.searchByDesription(description);
    }
}

package com.lufthansa.webapplication.model.dto;

import com.lufthansa.webapplication.entities.Role;
import com.sun.istack.NotNull;
import lombok.*;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class UserDto {
    @NotNull
    @NotEmpty(message = "Required field")
    private String firstName;
    private String lastName;
    @Email(message = "Not a valid email address")
    private String email;
    private String username;
    private String password;
    private Role role;
    @AssertTrue(message = "Password must contain at least 4 characters")
    public boolean validPassword() {
        return password.length() > 3 && !password.isBlank();
    }
}

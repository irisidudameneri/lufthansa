package com.lufthansa.webapplication.model.dto;


import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JobDto {
    private String jobName;
    private String description;
}

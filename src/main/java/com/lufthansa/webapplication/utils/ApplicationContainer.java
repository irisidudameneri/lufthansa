package com.lufthansa.webapplication.utils;

import com.lufthansa.webapplication.entities.Application;
import com.lufthansa.webapplication.entities.Job;
import com.lufthansa.webapplication.entities.JobApplication;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;
import static org.springframework.web.context.WebApplicationContext.SCOPE_SESSION;

@Component
@Scope(scopeName = SCOPE_SESSION, proxyMode = TARGET_CLASS)
public class ApplicationContainer {

        private Application applications;
        private Map<Long, JobApplication> jobApplications;

        public ApplicationContainer() {
            applications = new Application();
            jobApplications = new HashMap<>();
        }


    public Application getApplications() {
        return applications;
    }

    public List<JobApplication> getJobApplications() {

            return new LinkedList<>(jobApplications.values());
    }

        public void addApplication(Job job) {
            long jobId = job.getJobId();
            JobApplication tempApplication= jobApplications.get(jobId);
            if (tempApplication != null) {
                incrementQuantity(jobId);
            } else {
                tempApplication = new JobApplication();
                tempApplication.setApplication(applications);
                tempApplication.setJob(job);
                tempApplication.setAmountOfApplication(1);
                jobApplications.put(jobId, tempApplication);
            }
        }

        public void removeApplication(Long jobId) {
            jobApplications.remove(jobId);
        }


        public void clear() {
            jobApplications.clear();
        }

        public void incrementQuantity(long jobId) {
           int amount = jobApplications.get(jobId).getAmountOfApplication();
            jobApplications.get(jobId).setAmountOfApplication(++amount);
        }
}

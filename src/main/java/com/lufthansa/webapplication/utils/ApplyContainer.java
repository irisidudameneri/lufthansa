package com.lufthansa.webapplication.utils;

import com.lufthansa.webapplication.entities.Application;
import com.lufthansa.webapplication.entities.JobApplication;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ApplyContainer {
    private Application applications;
    private List<JobApplication> applicationList;
}

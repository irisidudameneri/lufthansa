package com.lufthansa.webapplication.repositories;


import com.lufthansa.webapplication.entities.Application;
import com.lufthansa.webapplication.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ApplicationRepository extends JpaRepository<Application,Long> {
    List<Application> findByUser (User user);
}

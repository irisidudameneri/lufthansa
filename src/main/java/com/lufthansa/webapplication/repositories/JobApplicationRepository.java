package com.lufthansa.webapplication.repositories;


import com.lufthansa.webapplication.entities.Application;
import com.lufthansa.webapplication.entities.JobApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JobApplicationRepository extends JpaRepository<JobApplication,Long> {
    List<JobApplication> findByApplication(Application application);
}

package com.lufthansa.webapplication.repositories;

import com.lufthansa.webapplication.entities.Job;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JobRepository extends JpaRepository<Job, Long> {

    @Query("Select j from Job j where j.description= :description")
    List<Job> searchByDesription(@Param("description") final String description);


}

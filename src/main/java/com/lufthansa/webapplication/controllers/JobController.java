package com.lufthansa.webapplication.controllers;
import com.lufthansa.webapplication.entities.Job;
import com.lufthansa.webapplication.model.dto.JobDto;
import com.lufthansa.webapplication.services.JobService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import java.util.List;


@Controller
public class JobController {
    @Autowired
    private  JobService jobService;



    @GetMapping("/user-home-page")
    public String findAllJobs(Model model){
        model.addAttribute("job",jobService.readAllJobs());
        return "user-home-page";
    }

    @GetMapping("/admin-home-page")
    public String showAdminForm(Model model) {
        List<Job> job = jobService.readAllJobs();
        model.addAttribute("job", job);
        return "admin-home-page";
    }

    @GetMapping("/addjob")
    public String showAddForm(JobDto jobDto) {
        return "job-add";
    }

    @PostMapping("/jobadd")
    public String postJob(JobDto jobDto, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "job-add";
        }
        jobService.saveJob(jobDto);
        model.addAttribute("job", jobService.readAllJobs());
        return "redirect:/admin-home-page";
    }
    @GetMapping("/edit/{jobId}")
    public String editJob(@PathVariable("jobId") long jobId, Model model) throws NotFoundException {
        Job job = jobService.findJobById(jobId);
        model.addAttribute("job", job);
        return "job-edit";
    }

    @PostMapping("/update/{jobId}")
    public String updateJob(@PathVariable("jobId") long jobId, JobDto jobDto, BindingResult result, Model model) throws NotFoundException {
        if (result.hasErrors()) {
            return "job-add";
        }
        jobService.updateJob(jobDto,jobId);
        model.addAttribute("job", jobService.readAllJobs());
        return "redirect:/admin-home-page";
    }
    @GetMapping("/delete/{jobId}")
    public String deleteJob(@PathVariable("jobId") long jobId, Model model) throws NotFoundException {
        jobService.deleteJobById(jobId);
        model.addAttribute("job",jobService.readAllJobs());
        return "redirect:/admin-home-page";
    }


    @GetMapping("/search")
    public String searchByDescription(Job job,Model model){
        List<Job>  jobs= jobService.searchByDescription(job.getDescription());
        model.addAttribute("jobs", jobs);
        return "search";
    }

}

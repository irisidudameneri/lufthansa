package com.lufthansa.webapplication.controllers;


import com.lufthansa.webapplication.entities.Application;
import com.lufthansa.webapplication.entities.User;
import com.lufthansa.webapplication.services.ApplicatonService;
import com.lufthansa.webapplication.services.JobService;
import com.lufthansa.webapplication.utils.ApplicationContainer;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class MainController {



    @Autowired
    private JobService jobService;

    @Autowired
    private ApplicationContainer applicationContainer;

    @Autowired
    private ApplicatonService applicatonService;

    @GetMapping("/login")
    public String showLoginPage() {
        return "login";
    }

    @GetMapping("/register")
    public String showRegisterPage(User user) {
        return "registration";
    }


    @RequestMapping("/user-home-page")
    public String indexPage(Model model) {
        model.addAttribute("jobs", jobService.readAllJobs());
        return "user-home-page";
    }

    @GetMapping("/cart/add/{jobId}")
    public String cartAddApplication(@PathVariable(value = "jobId") long jobId) throws NotFoundException {
        applicationContainer.addApplication(jobService.findJobById(jobId));
        System.out.println(applicationContainer);
        return "redirect:/user-home-page";
    }

    @GetMapping("/cart/remove/{jobId}")
    public String cartRemoveProduct(@PathVariable(value = "jobId") long jobId) {
        applicationContainer.removeApplication(jobId);
        return "redirect:/cart";
    }

    @GetMapping("/cart/clear")
    public String cartClear() {
        applicationContainer.clear();
        return "redirect:/cart";
    }



    @GetMapping("/cart")
    public String cartView() {
        return "cart";
    }

    @GetMapping("/cart/applications")
    public  String  showApplications(Model model){
        List<Application>applicationList = applicatonService.readAllTheApplications();
        model.addAttribute("applicationList",applicationList);
        return "applicationSubmit";
    }

    @PostMapping("/cart/add-applications")
    public String submitApplications(Model model) {
        model.addAttribute("applicationList", applicatonService.submitOrderForApplication());

        return "redirect:/user-home-page";
    }
}

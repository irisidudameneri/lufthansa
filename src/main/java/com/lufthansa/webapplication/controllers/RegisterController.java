package com.lufthansa.webapplication.controllers;

import com.lufthansa.webapplication.entities.Role;
import com.lufthansa.webapplication.entities.User;
import com.lufthansa.webapplication.repositories.RoleRepository;
import com.lufthansa.webapplication.repositories.UserRepository;
import com.lufthansa.webapplication.services.UserService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.Set;

@Controller
@RequestMapping("/registration")
public class RegisterController {

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
 
    @Autowired
    private UserService userService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private  UserRepository userRepository;
    @ModelAttribute("user")
    public User user() {
        return new User();
    }

    @GetMapping
    public String showRegistrationForm(User user) {
        return "registration";
    }

    @PostMapping
    public String registerUserAccount(@ModelAttribute("user") @Valid User user, BindingResult result) throws NotFoundException {
        User existing = userService.findByUsername(user.getUsername());
        if (existing != null){
            result.rejectValue("username", null, "There is already an account registered with that name");
        }

        if (result.hasErrors()){
            return "registration";
        }
        String encodedPassword = new BCryptPasswordEncoder().encode(user.getPassword());
        user.setPassword(encodedPassword);
        Role role = roleRepository.findById(1L).orElseThrow(() ->
                new NotFoundException("No role with such id is found")
        );
        user.setUserRole(Set.of(role));
        userRepository.save(user);
        return "redirect:/registration?success";
    }
}

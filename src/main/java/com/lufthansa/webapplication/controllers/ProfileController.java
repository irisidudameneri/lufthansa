package com.lufthansa.webapplication.controllers;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Optional;

@Controller
public class ProfileController {

    @GetMapping("/")
    public String root(Model model, @AuthenticationPrincipal UserDetails currentUser) {
        model.addAttribute("username", currentUser.getUsername());
        Optional<? extends GrantedAuthority> authorities = currentUser.getAuthorities().stream().findFirst();
        if (authorities.isPresent()) {
            GrantedAuthority role = authorities.get();
            if (role.getAuthority().equals("ROLE_ADMIN")) {
                return "admin-home-page";
            } else if (role.getAuthority().equals("ROLE_USER")) {
                return "user-home-page";
            }
        }
        return "/";
    }

}

package com.lufthansa.webapplication.controllers;
import com.lufthansa.webapplication.entities.User;
import com.lufthansa.webapplication.model.dto.UserDto;
import com.lufthansa.webapplication.services.ApplicatonService;
import com.lufthansa.webapplication.services.JobService;
import com.lufthansa.webapplication.services.UserService;
import com.lufthansa.webapplication.utils.ApplicationContainer;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;


@Controller
public class UserController {
    @Autowired
    private JobService jobService;

    @Autowired
    private UserService userService;
    @Autowired
    private ApplicatonService applicatonService;
    @Autowired
    private ApplicationContainer applicationContainer;


    @GetMapping("/edituser")
    public String editUser( Model model) throws NotFoundException {
        User user = userService.getLoggedInUser();
        model.addAttribute("user", user);
        return "user-edit";
    }
    @PostMapping("/updateuser")
    public String updateUser(UserDto userDto, BindingResult result, Model model) throws NotFoundException {
        if (result.hasErrors()) {
            return "user-edit";
        }
        userService.updateTheUser(userDto);
        model.addAttribute("user",jobService.readAllJobs());
        return "redirect:/user-home-page";
    }


    @GetMapping("/user-page")
    public String showHomeForm(Model model) {
        List<User> user = userService.readAllTheUsers();
        model.addAttribute("user", user);
        return "user-page";
    }

    @GetMapping("/edituser/{userId}")
    public String editUser(@PathVariable("userId") long userId, Model model) throws NotFoundException {
        User user = userService.readUserById(userId);
        model.addAttribute("user", user);
        return "user-edit";
    }
    @PostMapping("/updateuser/{userId}")
    public String updateUser(@PathVariable("userId") long userId, UserDto userDto, BindingResult result, Model model) throws NotFoundException {
        if (result.hasErrors()) {
            return "user-edit";
        }
        userService.updateTheUser(userDto);
        model.addAttribute("user", userService.readAllTheUsers());
        return "redirect:/user-page";
    }
    @GetMapping("/deleteuser/{userId}")
    public String deleteUser(@PathVariable("userId") long userId, Model model) throws NotFoundException {
        userService.deleteUserById(userId);
        model.addAttribute("user",userService.readAllTheUsers());
        return "redirect:/user-page";
    }

    @GetMapping(value = "/applications")
    public String checkApplications(Model model) {
        model.addAttribute("applicationList", applicatonService.getApplicationsUser());
        return "applicationList";
    }

   
}

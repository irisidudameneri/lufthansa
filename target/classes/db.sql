insert into role(role_id,role_name) values(1, 'ROLE_USER');
insert into role(role_id,role_name) values(2, 'ROLE_ADMIN');

-- username/password = username/USER1
-- username/password = admin/ADMIN1
insert into user(user_id, email, password,username) values(1,'user@gmail.com','$2y$12$qKcsCJHRdCugzQ52UcPeee4U/X9b.G0ia3kOrhd9dfrZ2xUC7zCR6','username');
insert into user(user_id, email, password,username) values(2,'admin@gmail.com','$2y$12$oNGeQgg9DOesjY.RuKXPqefn9/y3zojSsQSWGhwvLDnC0DLMK0SZK','admin');
insert into user_role values(1,1);
insert into user_role values(2,2);

INSERT INTO job (job_id, description, job_name) VALUES (1, 'java Se', 'Web Developer');
INSERT INTO job (job_id, description, job_name) VALUES (2,	'python', 'Web Developer');
INSERT INTO job (job_id, description, job_name) VALUES (3,	'python', 'Web Developer');
INSERT INTO job (job_id, description, job_name) VALUES (4,	'php',	'Web Developer');
INSERT INTO job (job_id, description, job_name) VALUES (5,	'c++',	'Web Developer');
INSERT INTO job (job_id, description, job_name) VALUES (6,	'html',	'Web Developer');
INSERT INTO job (job_id, description, job_name) VALUES (7,	'Manual tester'	,'Software Tester');

